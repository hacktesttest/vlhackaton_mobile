Авторизация через oAuth 
Передача токена на сервер и проверка
В случае успешной проверки добавить объекты 
----------------------
oauth_vk

POST
action: oauth_vk
token: %token_vk%

Response:
first_name (string)
last_name	(string)
vk_id		(string)
url_pic		(string)
year  (int)
city (string)

----------------------

Вывести список евентов последних 20
----------------------
last_events

POST
action: last_events
token: %token_vk%
offset: 0

Response:
array{
id{
	name
	low_people_count
	current_people_count
	high_people_count
	current_people
	begin_date
	end_date
	location
	tags
}
...
}

----------------------
Мои евенты.
----------------------
my_events

POST
action: my_events
token: %token_vk%


Response:
array{
id{
	name
	low_people_count
	current_people_count
	high_people_count
	begin_date
	end_date
	location
	tags
}
...
}

----------------------

---------------------
Открыть эвент ()
open_event

POST
action: open_event
token: %token_vk%
id: %n%

Response:

array{
	name
	low_people_count
	current_people_count
	high_people_count
	begin_date
	end_date
	location
	creator
	phone_creator
	desc
	money
	tags
}

------------------

------------------
Принять участие
agree_event

POST
action: agree_event
token: %token_vk%
id: %n%

Response

OK 200
------------------


------------------
Отменить участие
cancel_event

POST
action: cancel_event
token: %token_vk%
id: %n%

Response

OK 200
------------------
