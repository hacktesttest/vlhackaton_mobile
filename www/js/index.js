

/*Выход из приложения*/
function exitFromApp() {
    navigator.app.exitApp();
}


function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp*1000);
    var months = ['Янв','Фев','Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Ноябр','Дек'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date+','+month+' '+year+' '+hour+':'+min+':'+sec ;
    return time;
}

/*Приложение полностью инициализировалось*/

document.addEventListener("deviceready", appReady, false);
function appReady()
{           /*Событие на кнопку "Назад"*/
    document.addEventListener('backbutton', function(e){
        /* Если первая страница (Авторизация) */
        if ($.mobile.activePage.attr('id') == 'auth-page')
        {
            /*Выйти из приложения*/
            e.preventDefault();
            exitFromApp();
        }
        else {
            /*Вернуться на страницу позже*/
            navigator.app.backHistory();
        }
    }, false);
}

/* Инициализация приложения*/
$(document).on("mobileinit", function () {
    /*Подключаем fast-click для избежания задержки меджу нажатием в 300мс*/
    FastClick.attach(document.body);
});

/*Страница Подгружена*/
$(document).on("pageshow", function () {
    var header = $('[data-role=header]').outerHeight(); /*Размер хидера*/
    var panel = $('.ui-controlgroup-controls').height(); /*Размер меню*/
    var panelheight = panel - header; /*Новый размер меню*/
    $('.ui-panel').css({
        'top': header,
        'min-height': panelheight
    });
});






$(document).on("pageinit", "#info-page", function () {
    $(".btn-out").click(function () {
        $('.text-out').hide();
        $('#' + this.id + '-text.text-out').show();
        return false;
    });
});


$(document).on("pageshow", "#events-page", function () {
    function loadeventsform(events_list) {
        e_list = $.parseJSON(events_list);

        $.each(e_list, function (key) {
            event =
                '<li data-corners="false" data-shadow="false"  data-wrapperels="div"  class="ui-btn  ui-li-has-arrow ui-li item-event"><div class="ui-btn-inner ui-li">'+
                    '<ul class="block-1">'+
                    '<li class="title">' + e_list[key].name+'('+ e_list[key].low_people_count+'/'+e_list[key].high_people_count+')</li>'+
                    '</ul>'+
                    '<ul class="block-2">'+
                    '<li class="start">' + e_list[key].begin_date + '</li>'+
                    '<li class="stop">' + e_list[key].end_date + '</li>'+
                    '</ul>'+
                    '<ul class="block-3">'+
                    '<li class="tags">Money: ' + e_list[key].money + '</li>'+
                    '</ul>'+
                    '</li>';

            $('ul.ui-listview').append(event);
        });
        event = '<li data-corners="false" data-shadow="false"  data-wrapperels="div"  class="ui-btn  ui-li-has-arrow ui-li ui-li-last  item-event"><div class="ui-btn-inner ui-li">'+
        '<div class="load-next-list-event">Загрузить еще</div>'+
        '</div></li>';
        $('ul.ui-listview').append(event);
    }

   // $('ul.ui-listview').empty();

   var token = window.localStorage.getItem("vk_token");
    //var   token = '123';
   // var events_list = window.localStorage.getItem("events_list");
   var  params  = {};

    params['action'] = 'last_events';
    params['token'] = token;
    params['offset'] = 0;
        $.ajax({
            url: 'http://hackatontest.cloudcontrolled.com/json/',
            data: JSON.stringify(params),
            type: 'POST',
            dataType: "text",
            error: function () {
                alert('error');
            },
            success: function (data) {
                arr = $.parseJSON(data);
                if (arr.state == "error") {
                    $.mobile.changePage("#auth-page")
                } else {
                    window.localStorage.setItem("events_list", data);
                    loadeventsform(data);

                }
            }
        });
   // } else {
  //      loadeventsform(events_list);
  //  }

    return false;
});




/*URL Parser */
var url_parser={
    get_args: function (s) {
        var tmp=new Array();
        s=(s.toString()).split('&');
        for (var i in s) {
            i=s[i].split("=");
            tmp[(i[0])]=i[1];
        }
        return tmp;
    },
    get_args_cookie: function (s) {
        var tmp=new Array();
        s=(s.toString()).split('; ');
        for (var i in s) {
            i=s[i].split("=");
            tmp[(i[0])]=i[1];
        }
        return tmp;
    }
};

/*VK AUTH */

var plugin_vk = {
    wwwref: false,
    plugin_perms: "12",

    auth: function (force) {
        if (!window.localStorage.getItem("plugin_vk_token") || force || window.localStorage.getItem("plugin_vk_perms")!=plugin_vk.plugin_perms) {
            var authURL="https://oauth.vk.com/authorize?client_id=4272406&scope="+this.plugin_perms+"&redirect_uri=http://oauth.vk.com/blank.html&display=touch&response_type=code&display=mobile";
            this.wwwref = window.open(encodeURI(authURL), '_blank', 'location=no');
            this.wwwref.addEventListener('loadstop', this.auth_event_url);
        }
    },
    auth_event_url: function (event) {
        var tmp=(event.url).split("#");
        if (tmp[0]=='https://oauth.vk.com/blank.html' || tmp[0]=='http://oauth.vk.com/blank.html') {
            plugin_vk.wwwref.close();
            var tmp=url_parser.get_args(tmp[1]);

            var  params  = {};

            params['action'] = 'oauth_vk';
            params['token'] = tmp['code'];

            $.ajax({
                url: 'http://hackatontest.cloudcontrolled.com/json/',
                data: JSON.stringify(params),
                type: 'POST',
                dataType: 'json',
                /*При ошибке вывести сообщение*/

                error: function () {
                    alert('error');
                },

                /*При успешном запросе...*/
                success: function (data) {
                    /*Если запрос валидный то...*/
                    if (data.state == "success") {
                        /*Сохраняем выданный токен в локальное хранилище*/
                        window.localStorage.setItem("vk_perms", plugin_vk.plugin_perms);
                        window.localStorage.setItem("vk_token", data.token);
                        window.localStorage.setItem("vk_exp",data.expires_in);
                        window.localStorage.setItem("first_name", data.first_name);
                        window.localStorage.setItem("last_name", data.last_name);
                        window.localStorage.setItem("vk_user_id", data.vk_id);
                        window.localStorage.setItem("url_pic", data.url_pic);
                        window.localStorage.setItem("year", data.year);
                        window.localStorage.setItem("city", data.city);
                        /*Изменяем страницу*/
                        $.mobile.changePage("#events-page");
                        alert(tmp['code']);


                    } else {
                        /*При ошибке вывести сообщение и очистить поля ввода*/
                        alert('Неправильный логин или пароль!');
                        window.localStorage.clear();
                    }
                    return false;
                }
            });

        }
    }
};